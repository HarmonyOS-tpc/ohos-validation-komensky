# ohos-validation-komensky
ohos-validation-komensky : A simple library for validating user input in forms using annotations.
# ohos-validation-komensky includes :
 - Validate **all views at once** and show feedback to user. _With one line of code._
 - **Live validation** - check user input as he moves between views with immediate feedback.
 - **Extensible** library - you can add your own validations or adapters for custom views.
 
# Usage Instructions
A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.
### How to validate

First, annotate your views like this:
```java
@NotEmpty(messageId = R.string.validation_name)
@MinLength(value = 3, messageId = R.string.validation_name_length, order = 2)
private EditText mNameEditText;
```

Now you are ready to:
```java
FormValidator.validate(this, new SimpleErrorPopupCallback(this, true, element));
```

You will receive collection of all failed validations in a callback and you can present them to the user as you want.
Or simply use prepared callbacks (like `SimpleErrorPopupCallback`).

### Live validation

To start and stop live validation, simply call:
```java
FormValidator.startLiveValidation(this, formContainer, new SimpleErrorPopupCallback(this, false, element));
FormValidator.stopLiveValidation(this);
```

### List of all supported validation annotations

Validations supported out of the box:
 - [NotEmpty](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/NotEmpty.java)

```java
@NotEmpty(messageId = R.string.validation_name, order = 1)
private EditText mNameEditText;
```
 - [MaxLength](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MaxLength.java)
 - [MinLength](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MinLength.java)

```java
@MinLength(value = 1, messageId = R.string.validation_participants, order = 2)
private EditText mNameEditText;
```
 - [MaxValue](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MaxValue.java)
 - [MinValue](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MinValue.java)

```java
@MinValue(value = 2L, messageId = R.string.validation_name_length)
private EditText mEditNumberOfParticipants;
```
 - [MaxNumberValue](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MaxNumber.java)
 - [MinNumberValue](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/MinNumberValue.java)

```java
@MinNumberValue(value = "5.5", messageId = R.string.validation_name_length)
private EditText mEditPotentialOfHydrogen;
```
 - [RegExp](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/RegExp.java)

```java
@RegExp(value = EMAIL, messageId = R.string.validation_valid_email)
private EditText mEditEmail;
@RegExp(value = "^[0-9]+$", messageId = R.string.validation_valid_count)
private EditText mEditCount;
```
 - [DateInFuture](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/DateInFuture.java)

```java
@DateInFuture(messageId = R.string.validation_date)
private TextView mTxtDate;
```
 - [DateNoWeekend](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/DateNoWeekend.java)

```java
@DateNoWeekend(messageId = R.string.validation_date_weekend)
private TextView mTxtDate;
```
 - [Custom](../master/library/src/main/java/eu/inmite/harmony/lib/validations/form/annotations/Custom.java)

```java
@Custom(value = MyVeryOwnValidator.class, messageId = R.string.validation_custom)
private EditText mNameEditText;
```

# Installation instructions

1. For using ohos-validation-komensky module in your sample application, add below dependencies:
	```
    dependencies {
    	implementation project(':library')
    	testImplementation 'junit:junit:4.13'
    	ohosTestImplementation 'com.huawei.ohos.testkit:runner:1.0.0.100'
	}
    ```

2. For using ohos-validation-komensky in separate application, add the below dependencies and include "library.har" in libs folder of "entry" module  :
	```
    dependencies {
    	implementation files('libs/library.har')
	}
    ```

3. For using ohos-validation-komensky from a remote repository in separate application, add the below dependencies in build.gradle of "entry" module  :
	```
    dependencies {
    	  implementation 'io.openharmony.tpc.thirdlib:ohos-validation-komensky:1.0.1'
	}
    ```