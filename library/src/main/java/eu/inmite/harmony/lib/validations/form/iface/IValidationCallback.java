package eu.inmite.harmony.lib.validations.form.iface;

import eu.inmite.harmony.lib.validations.form.FormValidator;

import ohos.agp.components.Component;

import java.util.List;

/**
* @author Tomas Vondracek
*/
public interface IValidationCallback {
    /**
     * Validation completed callback.
     *
     * @param result true if validation passed
     * @param failedValidations collections of all failed validations, this collection should never be null and is
     *                          immutable
     * @param passedValidations passedValidations
     */
    void validationComplete(boolean result, List<FormValidator.ValidationFail> failedValidations,
        List<Component> passedValidations);
}
