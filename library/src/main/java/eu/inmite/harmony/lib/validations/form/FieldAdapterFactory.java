package eu.inmite.harmony.lib.validations.form;

import eu.inmite.harmony.lib.validations.form.adapters.CompoundAdapter;
import eu.inmite.harmony.lib.validations.form.adapters.JoinedAdapter;
import eu.inmite.harmony.lib.validations.form.adapters.SpinnerAdapter;
import eu.inmite.harmony.lib.validations.form.adapters.TextViewAdapter;
import eu.inmite.harmony.lib.validations.form.annotations.Joined;
import eu.inmite.harmony.lib.validations.form.iface.IFieldAdapter;

import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * Created adapters for views with validation.
 *
 * @author Tomas Vondracek
 */
public class FieldAdapterFactory {
    private static JoinedAdapter sJoinedAdapter;

    private static TextViewAdapter sTextViewAdapter;

    private static SpinnerAdapter sSpinnerViewAdapter;

    private static CompoundAdapter sCompoundViewAdapter;

    private static Map<Class<? extends Component>, IFieldAdapter<? extends Component, ?>> sExternalAdapters;

    private FieldAdapterFactory() {
    }

    static void registerAdapter(Class<? extends Component> viewType,
        Class<? extends IFieldAdapter<? extends Component, ?>> adapterClazz)
        throws IllegalAccessException, InstantiationException {
        if (sExternalAdapters == null) {
            sExternalAdapters = new HashMap<>();
        }
        sExternalAdapters.put(viewType, adapterClazz.newInstance());
    }

    /**
     * getAdapterForField()
     *
     * @param view component
     * @return fileAdapter
     */
    public static IFieldAdapter getAdapterForField(Component view) {
        return getAdapterForField(view, null);
    }

    /**
     * getAdapterForField()
     *
     * @param view component
     * @param annotation annotation
     * @return fileAdapter
     */
    public static IFieldAdapter<? extends Component, ?> getAdapterForField(Component view, Annotation annotation) {
        final IFieldAdapter<? extends Component, ?> adapter;
        if (annotation != null && Joined.class.equals(annotation.annotationType())) {
            if (sJoinedAdapter == null) {
                sJoinedAdapter = new JoinedAdapter();
            }
            adapter = sJoinedAdapter;
        } else if (sExternalAdapters != null && sExternalAdapters.containsKey(view.getClass())) {
            adapter = sExternalAdapters.get(view.getClass());
        } else if (view instanceof Checkbox) {
            if (sCompoundViewAdapter == null) {
                sCompoundViewAdapter = new CompoundAdapter();
            }
            adapter = sCompoundViewAdapter;
        } else if (view instanceof Text) {
            if (sTextViewAdapter == null) {
                sTextViewAdapter = new TextViewAdapter();
            }
            adapter = sTextViewAdapter;
        } else {
            adapter = null;
        }
        return adapter;
    }

    static void clear() {
        if (sExternalAdapters != null) {
            sExternalAdapters.clear();
        }
        sJoinedAdapter = null;
        sTextViewAdapter = null;
        sSpinnerViewAdapter = null;
    }
}
