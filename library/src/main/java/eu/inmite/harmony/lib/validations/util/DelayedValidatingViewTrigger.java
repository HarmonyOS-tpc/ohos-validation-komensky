package eu.inmite.harmony.lib.validations.util;

import eu.inmite.harmony.lib.validations.form.FormValidator;
import eu.inmite.harmony.lib.validations.form.iface.IValidationCallback;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.lang.ref.WeakReference;

/**
 * DelayedValidatingViewTrigger
 */
public class DelayedValidatingViewTrigger {
    /**
     * DEFAULT_DELAY
     */
    public static final int DEFAULT_DELAY = 1000;

    private final WeakReference<Fraction> fragmentRef;

    private final WeakReference<Component> targetViewRef;

    private final WeakReference<IValidationCallback> callbackRef;

    private final DelayedHandler handler;

    private final int delay;

    /**
     * Constructor
     *
     * @param fragment Fraction
     * @param targetView targetView
     * @param callback callback
     */
    public DelayedValidatingViewTrigger(Fraction fragment, Component targetView, IValidationCallback callback) {
        this(fragment, targetView, callback, DEFAULT_DELAY);
    }

    /**
     * Constructor
     * \
     *
     * @param fragment Fraction
     * @param targetView targetView
     * @param callback callback
     * @param delay delay
     */
    public DelayedValidatingViewTrigger(Fraction fragment, Component targetView, IValidationCallback callback,
        int delay) {
        fragmentRef = new WeakReference<>(fragment);
        targetViewRef = new WeakReference<>(targetView);
        callbackRef = new WeakReference<>(callback);
        this.delay = delay;
        handler = new DelayedHandler(EventRunner.getMainEventRunner());
    }

    /**
     * run()
     */
    public void run() {
        handler.removeEvent(DelayedHandler.REQUEST);
        handler.sendEvent(DelayedHandler.REQUEST, delay);
    }

    /**
     * DelayedHandler
     */
    private class DelayedHandler extends EventHandler {
        /**
         * REQUEST
         */
        public static final int REQUEST = 1;

        /**
         * Constructor
         *
         * @param runner EventRunner
         * @throws IllegalArgumentException exception
         */
        public DelayedHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            switch (event.eventId) {
                case REQUEST:
                    // check if target fragment is not destroyed
                    Fraction fragment = fragmentRef.get();
                    if (fragment == null) {
                        return;
                    }
                    // try validateSingleView
                    Component targetView = targetViewRef.get();
                    IValidationCallback callback = callbackRef.get();
                    if (targetView != null) {
                        FormValidator.validateSingleView(fragment, targetView, callback);
                    } else {
                        FormValidator.validate(fragment, callback);
                    }
                    break;
            }
        }
    }
}
