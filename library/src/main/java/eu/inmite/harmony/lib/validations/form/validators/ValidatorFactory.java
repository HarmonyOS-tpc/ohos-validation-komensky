package eu.inmite.harmony.lib.validations.form.validators;

import eu.inmite.harmony.lib.validations.form.annotations.ValidatorFor;
import eu.inmite.harmony.lib.validations.form.iface.IValidator;

import ohos.utils.LruBuffer;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomas Vondracek
 */
public class ValidatorFactory {
    private static final int INSTANCE_CACHE_SIZE = 4;

    private static final LruBuffer<Class<? extends IValidator>, IValidator> sCachedValidatorInstances = new LruBuffer<>(
        INSTANCE_CACHE_SIZE);

    private static final Map<Class<? extends Annotation>, Class<? extends IValidator>> sValidators = new HashMap<>();

    static {
        // our default validators:

        registerValidatorClasses(CustomValidator.class, LengthValidator.class, NumberValueValidator.class,
            LengthValidator.class, ValueValidator.class, NumberValueValidator.class, LengthValidator.class,
            ValueValidator.class, NotEmptyValidator.class, WeekendDateValidator.class, FutureDateValidator.class,
            RegExpValidator.class, CheckedValidator.class);
    }

    private ValidatorFactory() {
    }

    /**
     * registerValidatorClasses()
     *
     * @param classes classes
     */
    public static void registerValidatorClasses(Class<? extends IValidator<?>>... classes) {
        if (classes == null || classes.length == 0) {
            return;
        }

        for (Class<? extends IValidator<?>> clazz : classes) {
            final Annotation[] annotations = clazz.getAnnotations();

            // search for @ValidatorFor annotation and read supported validations
            for (Annotation annotation : annotations) {
                if (annotation instanceof ValidatorFor) {
                    Class<? extends Annotation>[] validationAnnotations = ((ValidatorFor) annotation).value();
                    for (Class<? extends Annotation> validationAnnotation : validationAnnotations) {
                        sValidators.put(validationAnnotation, clazz);
                    }
                    break;
                }
            }
        }
    }

    /**
     * getValidator()
     *
     * @param annotation annotation
     * @return validator
     * @throws IllegalAccessException IllegalAccessException
     * @throws InstantiationException InstantiationException
     */
    public static IValidator getValidator(Annotation annotation) throws IllegalAccessException, InstantiationException {
        if (annotation == null) {
            return null;
        }

        final Class<? extends IValidator> clazz = sValidators.get(annotation.annotationType());

        IValidator validator = null;
        if (clazz != null) {
            validator = sCachedValidatorInstances.get(clazz);
            if (validator == null) {
                validator = clazz.newInstance();
                sCachedValidatorInstances.put(clazz, validator);
            }
        }
        return validator;
    }

    /**
     * clearCachedValidators()
     */
    public static void clearCachedValidators() {
        sCachedValidatorInstances.clear();
    }
}
