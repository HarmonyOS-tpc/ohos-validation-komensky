package eu.inmite.harmony.lib.validations.form.iface;

/**
 * @author Tomas Vondracek
 */
public interface ICondition<T> {
    /**
     * evaluate ()
     *
     * @param input input
     * @return boolean
     */
    boolean evaluate(T input);
}
