package eu.inmite.harmony.lib.validations.form.callback;

import eu.inmite.harmony.lib.validations.form.FormValidator;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.app.Context;

import java.util.Collection;

/**
 * Callback that will show error text on first view with failed validation
 *
 * @author Tomas Vondracek
 */
public class SimpleErrorPopupCallback extends SimpleToastCallback {
    private final Element mErrorIcon;

    /**
     * Constructor
     *
     * @param context context
     */
    public SimpleErrorPopupCallback(Context context) {
        this(context, false, null);
    }

    /**
     * Constructor
     *
     * @param context context
     * @param focusFirstFail focusFirstFail
     */
    public SimpleErrorPopupCallback(Context context, boolean focusFirstFail) {
        this(context, focusFirstFail, null);
    }

    /**
     * Constructor
     *
     * @param context context
     * @param focusFirstFail focusFirstFail
     * @param errorIcon error icon
     */
    public SimpleErrorPopupCallback(Context context, boolean focusFirstFail, Element errorIcon) {
        super(context, focusFirstFail);
        mErrorIcon = errorIcon;
    }

    @Override
    protected void showValidationMessage(FormValidator.ValidationFail firstFail) {
        if (firstFail.view instanceof Text) {
            final Text txt = (Text) firstFail.view;
            if (mErrorIcon != null) {
                txt.setAroundElements(null, null, mErrorIcon, null);
                txt.setHint(firstFail.message);
            }
        } else {
            super.showValidationMessage(firstFail);
        }
    }

    @Override
    protected void showViewIsValid(Collection<Component> passedValidations) {
        for (Component view : passedValidations) {
            if (view instanceof Text) {
                ((Text) view).setAroundElements(null, null, null, null);
            }
        }
    }
}
