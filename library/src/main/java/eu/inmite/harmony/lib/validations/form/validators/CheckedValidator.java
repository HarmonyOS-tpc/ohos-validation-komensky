package eu.inmite.harmony.lib.validations.form.validators;

import eu.inmite.harmony.lib.validations.form.annotations.Checked;
import eu.inmite.harmony.lib.validations.form.annotations.ValidatorFor;

import java.lang.annotation.Annotation;

/**
 * CheckedValidator
 */
@ValidatorFor( {Checked.class})
public class CheckedValidator extends BaseValidator<Boolean> {
    @Override
    public boolean validate(Annotation annotation, Boolean input) {
        return input;
    }
}
