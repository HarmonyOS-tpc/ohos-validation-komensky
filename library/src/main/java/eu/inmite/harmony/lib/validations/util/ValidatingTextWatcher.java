package eu.inmite.harmony.lib.validations.util;

import eu.inmite.harmony.lib.validations.form.iface.IValidationCallback;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * ValidatingTextWatcher
 */
public class ValidatingTextWatcher extends DelayedValidatingViewTrigger implements Text.TextObserver {
    /**
     * Constructor
     *
     * @param fragment Fraction
     * @param targetView targetVeiw
     * @param callback callback
     */
    public ValidatingTextWatcher(Fraction fragment, Component targetView, IValidationCallback callback) {
        super(fragment, targetView, callback);
    }

    /**
     * Constructor
     *
     * @param fragment Fraction
     * @param targetView targetVeiw
     * @param callback callback
     * @param delay delay
     */
    public ValidatingTextWatcher(Fraction fragment, Component targetView, IValidationCallback callback, int delay) {
        super(fragment, targetView, callback, delay);
    }

    @Override
    public void onTextUpdated(String string, int i1, int i2, int i3) {
        run();
    }
}
