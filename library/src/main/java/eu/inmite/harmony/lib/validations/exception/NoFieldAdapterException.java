package eu.inmite.harmony.lib.validations.exception;

import ohos.agp.components.Component;

import java.lang.annotation.Annotation;

/**
 * @author Tomas Vondracek
 */
public class NoFieldAdapterException extends FormsValidationException {
    /**
     * Constructor
     *
     * @param component component
     * @param annotation annotation
     */
    public NoFieldAdapterException(final Component component, final Annotation annotation) {
        super("no adapter was found for view " + component + " with annotation " + annotation);
    }
}
