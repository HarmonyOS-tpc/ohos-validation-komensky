package eu.inmite.harmony.lib.validations.form.callback;

import eu.inmite.harmony.lib.validations.form.FormValidator;
import eu.inmite.harmony.lib.validations.form.iface.IValidationCallback;

import ohos.agp.components.Component;
import ohos.app.Context;

import java.util.Collection;
import java.util.List;

/**
 * @author Tomas Vondracek
 */
public abstract class SimpleCallback implements IValidationCallback {
    /**
     * Context
     */
    protected final Context mContext;

    /**
     * Flag for first fail focus
     */
    protected final boolean mFocusFirstFail;

    /**
     * Constructor
     *
     * @param context context
     * @param focusFirstFail focusFirstFail
     */
    public SimpleCallback(Context context, boolean focusFirstFail) {
        mFocusFirstFail = focusFirstFail;
        mContext = context;
    }

    @Override
    public void validationComplete(boolean result, List<FormValidator.ValidationFail> failedValidations,
        List<Component> passedValidations) {
        if (!failedValidations.isEmpty()) {
            FormValidator.ValidationFail firstFail = failedValidations.get(0);
            if (mFocusFirstFail) {
                firstFail.view.requestFocus();
            }
            showValidationMessage(firstFail);
        }

        if (!passedValidations.isEmpty()) {
            showViewIsValid(passedValidations);
        }
    }

    /**
     * present validation message to the user
     *
     * @param firstFail firstFail
     */
    protected abstract void showValidationMessage(FormValidator.ValidationFail firstFail);

    /**
     * present to user that passed views are valid
     *
     * @param passedValidations all views that passed the validation
     */
    protected void showViewIsValid(Collection<Component> passedValidations) {
        // nothing
    }
}
