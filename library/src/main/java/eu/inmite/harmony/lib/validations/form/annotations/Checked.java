package eu.inmite.harmony.lib.validations.form.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validate input string that it's not empty.
 * @author Tomas Vondracek
 */
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Checked {
    /**
     * value()
     *
     * @return true
     */
    boolean value() default true;

    /**
     * messageId()
     *
     * @return 0
     */
    int messageId() default 0;

    /**
     * order()
     *
     * @return order
     */
    int order() default 1000;
}
