package eu.inmite.harmony.lib.validations.form.callback;

import eu.inmite.harmony.lib.validations.form.FormValidator;

import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * Validation callback that will show toast for first validation fail
 *
 * @author Tomas Vondracek
 */
public class SimpleToastCallback extends SimpleCallback {
    /**
     * Constructor
     *
     * @param context context
     */
    public SimpleToastCallback(Context context) {
        this(context, false);
    }

    /**
     * Constructor
     *
     * @param context context
     * @param focusFirstFail focusFirstFail
     */
    public SimpleToastCallback(Context context, boolean focusFirstFail) {
        super(context, focusFirstFail);
    }

    @Override
    protected void showValidationMessage(FormValidator.ValidationFail firstFail) {
        ToastDialog toastDialog = new ToastDialog(mContext);
        toastDialog.setText(firstFail.message).show();
    }
}
