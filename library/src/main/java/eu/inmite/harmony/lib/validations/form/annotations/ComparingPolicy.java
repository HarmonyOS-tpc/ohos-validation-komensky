package eu.inmite.harmony.lib.validations.form.annotations;

/**
 * Policy for comparing number values.
 *
 * @author Tomas Vondracek
 */
public enum ComparingPolicy {
    /**
     * INCLUSIVE
     */
    INCLUSIVE,
    /**
     * EXCLUSIVE
     */
    EXCLUSIVE
}
