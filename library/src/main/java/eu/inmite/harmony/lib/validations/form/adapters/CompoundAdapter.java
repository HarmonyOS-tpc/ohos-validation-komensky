package eu.inmite.harmony.lib.validations.form.adapters;

import eu.inmite.harmony.lib.validations.form.annotations.Checked;
import eu.inmite.harmony.lib.validations.form.iface.IFieldAdapter;

import ohos.agp.components.Checkbox;

import java.lang.annotation.Annotation;

/**
 * CompoundAdapter
 */
public class CompoundAdapter implements IFieldAdapter<Checkbox, Boolean> {
    @Override
    public Boolean getFieldValue(Annotation annotation, Checkbox fieldView) {
        return ((Checked) annotation).value() == fieldView.isChecked();
    }
}
