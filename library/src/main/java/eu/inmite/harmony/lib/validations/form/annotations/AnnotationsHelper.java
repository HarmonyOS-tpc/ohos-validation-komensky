package eu.inmite.harmony.lib.validations.form.annotations;

import java.lang.annotation.Annotation;

/**
 * Since annotations don't support inheritance, we use convention and this helper get values from annotation by
 * convention.
 * @author Tomas Vondracek
 */
public class AnnotationsHelper {
    /**
     * VALUE_NAME
     */
    public static final String VALUE_NAME = "value";

    private AnnotationsHelper() {
    }

    /**
     * Gets the annotation value
     *
     * @param annotation annotation
     * @return value
     */
    public static Object getAnnotationValue(Annotation annotation) {
        return getAnnotationValueWithName(annotation, VALUE_NAME);
    }

    /**
     * Gets the annotation value with given name
     *
     * @param annotation annotation
     * @param valueName valueName
     * @return value
     */
    public static Object getAnnotationValueWithName(Annotation annotation, String valueName) {
        try {
            return annotation.annotationType().getMethod(valueName).invoke(annotation);
        } catch (Exception e) {
            return null;
        }
    }
}
