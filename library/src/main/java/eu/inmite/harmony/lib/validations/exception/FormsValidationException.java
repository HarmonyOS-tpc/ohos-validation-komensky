package eu.inmite.harmony.lib.validations.exception;

/**
 * @author Tomas Vondracek
 */
public class FormsValidationException extends RuntimeException {
    /**
     * Constructor
     */
    public FormsValidationException() {
    }

    /**
     * Constructor
     *
     * @param detailMessage message
     */
    public FormsValidationException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructor
     *
     * @param detailMessage message
     * @param throwable throwable
     */
    public FormsValidationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructor
     *
     * @param throwable throwable
     */
    public FormsValidationException(Throwable throwable) {
        super(throwable);
    }
}
