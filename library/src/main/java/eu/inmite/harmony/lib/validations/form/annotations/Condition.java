package eu.inmite.harmony.lib.validations.form.annotations;

import eu.inmite.harmony.lib.validations.form.iface.ICondition;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation makes conditioned validation possible. It can be attached to single validation or all on field.
 *
 * @author Tomas Vondracek
 */
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Condition {
    /**
     * value()
     *
     * @return condition value
     */
    Class<? extends ICondition> value();

    /**
     * if there is no target annotation mentioned we will apply condition for all annotations
     *
     * @return annotation
     */
    Class<? extends Annotation> validationAnnotation() default Condition.class;

    /**
     * viewId()
     *
     * @return id
     */
    int viewId();
}
