/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.inmite.demo.validations;

import eu.inmite.harmony.lib.validations.form.FormValidator;
import eu.inmite.harmony.lib.validations.form.annotations.MinValue;
import eu.inmite.harmony.lib.validations.form.callback.SimpleErrorPopupCallback;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.app.Context;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * FormValidationNumberValue
 */
public class FormValidationNumberValue {
    private Context mContext;

    private Component rootLayout;

    @MinValue(value = 1L)
    private TextField field;

    private Element errorIcon;

    /**
     * setUp
     */
    @Before
    public void setUp() {
        mContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        rootLayout = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_name, null, false);
        field = (TextField) rootLayout.findComponentById(ResourceTable.Id_demo_name);
    }

    /**
     * setText
     * @param text text
     */
    public void setText(String text) {
        field.setText(text);
    }

    /**
     * testValidationFail
     */
    @Test
    public void testValidationFail() {
        setText("name");
        boolean isValid = FormValidator.validate(mContext, this,
            new SimpleErrorPopupCallback(mContext, true, errorIcon));
        Assert.assertFalse(isValid);
    }

    /**
     * testValidationPass
     */
    @Test
    public void testValidationPass() {
        setText("2");
        boolean isValid = FormValidator.validate(mContext, this,
            new SimpleErrorPopupCallback(mContext, true, errorIcon));
        Assert.assertTrue(isValid);
    }
}
